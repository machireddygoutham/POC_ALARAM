#Main alarm program
#-------------------------------------------------------------------------------
import winsound, threading, time

alarams = {}

class alarmNotif(object):
    def __init__(self):
        thread = threading.Thread(target=self.run, args=())
        #thread.daemon = True
        thread.start()

    def run(self):
        flag = False
        while True:
            conts = alarams.copy()
            if alarams:
                for item in conts:
                    ringer = conts[item][1]
                    ctime = time.strftime("%H:%M")
                    while ctime == ringer:
                        ctime = time.strftime("%H:%M")
                        winsound.PlaySound("sound.wav", winsound.SND_FILENAME)
                        flag = True
                        active = item
                if flag: print "\n\nAlaram with Id:%s is on\nContinue with your previous task if any\n"%active;flag = False



# -----------------------------------------------------------------------------------------------------------------------

def addAlaram(id, title, time, rpt):
    if id not in alarams:
        alarams[id] = [title, time, rpt]
        print "Created alaram"
        return 1  # Created alarams
    else:
        print "Alaram already present"
        return 0  # Alaram already present


def updateAlaram(id, title, time, rpt):
    if id in alarams:
        alarams[id] = [title, time, rpt]
        return 1  # alaram updated
    else:
        addAlaram(id, title, time, rpt)
        return 0  # alaram added


def deleteAlaram(id):
    if id in alarams:
        del alarams[id]
        return 0  # alaram deleted
    else:
        return 0  # alaram not present


def deleteAll():
    alarams.clear()
    return 0  # alarams deleted


def alaramCount():
    count = 0
    for x in alarams:
        count += 1
    return count


def alaramDisplay(id):
    if id in alarams:
        print alarams[id]
    return None


def ringAlaram():
    ringer = raw_input("Enter")
    #ringer = alarams[id][1]
    while True:
        ctime = time.strftime("%H:%M")
        if ctime == ringer:
            print "played"
            winsound.PlaySound("sound.wav", winsound.SND_FILENAME)
            break
    print "completed"
    return None


def continuer():
    example = alarmNotif()
    while True:
        input = raw_input("1) Enter a new alaram details\n"
                          "2) Update an alaram\n"
                          "3) Remove an alaram\n"
                          "4) Clear all alarams\n"
                          "5) Count of all alarams\n"
                          "6) Details of an alaram by its id\n"
                          "Enter empty string to exit")
        if input == "1":
            id = raw_input("Please enter an id for the alaram")
            title = raw_input("Please enter a title for the alaram")
            time = raw_input("Please enter time in the format HH:MM")
            rpt = raw_input("Please enter the repetion for alaram")
            addAlaram(id, title, time, rpt)
        elif input == "2":
            print "If alaram is not present a new alaram will be created!!!"
            id = raw_input("Please enter an id for the alaram change")
            title = raw_input("Please enter a title for the alaram change")
            time = raw_input("Please enter time in the format HH:MM change")
            rpt = raw_input("Please enter the repetion for alaram change")
            updateAlaram(id, title, time, rpt)
        elif input == "3":
            id = raw_input("Please enter an id for the alaram to delete")
            deleteAlaram(id)
            print "The alaram with id: %s is successfully deleted"%id
        elif input == "4":
            deleteAll()
            print "All the alarams are successfully deleted"
        elif input == "5":
            count = alaramCount()
            print "There are a total of %s alrams in the queue"%count
        elif input == "6":
            id = raw_input("Please enter an id for the alaram to display")
            print "Displaying the alaram"
            alaramDisplay(id)
        elif input == "":
            exit()

#As we are executing the program through test cases the 2nd below line is not required
#If user wants to execute the program straight then uncommand the below line

#continuer()
